use std::fs::{read_dir, DirEntry};
use std::path::Path;

pub fn directories_in(path: impl AsRef<Path>)
-> eyre::Result<impl Iterator<Item = DirEntry>> {
    let result = read_dir(path.as_ref())?
        .into_iter()
        .map(|x| x.expect("read_dir entry failed"))
        .filter(|x| x.file_type().expect("read_dir file type failed").is_dir());

    Ok(result)
}

pub fn directories_in_recursive(path: impl AsRef<Path>)
-> eyre::Result<Vec<DirEntry>> {
    let path = path.as_ref();
    let mut result = directories_in(path)?.collect::<Vec<_>>();
    for child in directories_in(path)? {
        for grandchild in directories_in(child.path())? {
            result.push(grandchild);
        }
    }

    Ok(result)
}
