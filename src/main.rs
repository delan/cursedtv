mod fs;

use std::collections::BTreeMap;
use std::env::args_os;
use std::fs::{read_dir, File, DirEntry};
use std::io::{Write, BufWriter, BufReader, Read};
use std::path::{PathBuf, Path};
use std::str::FromStr;
use std::time::Duration;

use eyre::{eyre, bail};
use itertools::Itertools;
use mediainfo::{MediaInfoError, MediaInfo};
use rand::seq::SliceRandom;
use rand::{thread_rng, random};
use serde::{Serialize, Deserialize};

use crate::fs::{directories_in_recursive, directories_in};

#[derive(Debug, Serialize, Deserialize)]
struct Config {
    version: usize,
    shows: BTreeMap<String, Show>,
    ads: Vec<Video>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Show {
    name: String,
    episodes: Vec<Video>,
}

#[derive(Debug, Serialize, Deserialize)]
struct Video {
    path: String,
    duration: Duration,
}

impl Video {
    fn episode(&self, root: &str, show_name: &str, start: Duration, stop: Duration) -> String {
        let path = format!("{}/{}/{}", root, show_name, self.path);
        let path = path.replace("&", "&amp;").replace("<", "&lt;");

        format!(r#"        <track>
            <title>[show] {path} {start}..{stop}</title>
            <location>{path}</location>
            <extension application="http://www.videolan.org/vlc/playlist/0">
                <vlc:option>start-time={start}</vlc:option>
                <vlc:option>stop-time={stop}</vlc:option>
            </extension>
        </track>"#, path = path, start = start.as_secs(), stop = stop.as_secs())
    }

    fn ad(&self, root: &str) -> String {
        let path = format!("{}/{}", root, self.path);
        let path = path.replace("&", "&amp;").replace("<", "&lt;");

        format!(r#"        <track>
            <title>[ad] {path}</title>
            <location>{path}</location>
        </track>"#, path = path)
    }
}

macro_rules! debug_dbg {
    ($($tt:tt)*) => {
        (if cfg!(debug_assertions) {
            dbg!($($tt)*)
        } else {
            ($($tt)*)
        })
    };
}

fn main() -> eyre::Result<()> {
    let config = if let Ok(json) = File::open("cursedtv.json") {
        let mut json = BufReader::new(json);
        let mut input = vec![];
        json.read_to_end(&mut input)?;

        let result: Config = serde_json::from_slice(&input)?;
        assert_eq!(result.version, 1);

        result
    } else {
        generate_index()?
    };

    dbg!(&config);
    let mut args = args_os().skip(1);
    let shows_root = args.next().unwrap();
    let shows_root = Path::new(&shows_root); // FIXME print usage
    let ads_root = args.next().unwrap();
    let ads_root = Path::new(&ads_root); // FIXME print usage

    println!(r#"<?xml version="1.0" encoding="UTF-8"?>
<playlist version="1" xmlns="http://xspf.org/ns/0/" xmlns:vlc="http://www.videolan.org/vlc/playlist/ns/0/">
    <trackList>"#);

    let mut indices: BTreeMap<&str, usize> = config.shows.keys().map(|x| (x.as_ref(), 0)).collect();
    let total_episodes = config.shows.values().fold(0, |a, x| a + x.episodes.len());
    let mut show_names: Vec<&str> = config.shows.keys().map(|x| x.as_ref()).collect();
    show_names.shuffle(&mut thread_rng());
    let mut show_names = show_names.iter().cycle();

    debug_dbg!(&indices);
    for _ in 0..total_episodes {
        let show_name = loop {
            let result = *show_names.next().unwrap();
            if *indices.get(result).unwrap() < config.shows.get(result).unwrap().episodes.len() {
                break result;
            }
        };
        let episode = indices.get_mut(show_name).unwrap();

        for part in 0..3 {
            // TODO bumper-ad-bumper between episodes, multiple ads otherwise
            let ad = random::<usize>() % config.ads.len();
            let ad = &config.ads[ad];
            println!("{}", ad.ad(ads_root.to_str().unwrap())); // FIXME non-unicode

            let episode = &config.shows.get(show_name).unwrap().episodes[*episode];
            let start = episode.duration / 3 * part;
            let stop = episode.duration / 3 * (part + 1);
            println!("{}", episode.episode(shows_root.to_str().unwrap(), show_name, start, stop)); // FIXME non-unicode
        }

        *episode += 1;
    }

    println!(r#"    </trackList>
    </playlist>"#);

    Ok(())
}

fn generate_index() -> eyre::Result<Config> {
    let usage = "usage: cursedtv <path/to/shows> <path/to/ads>";
    let mut args = args_os().skip(1);

    let path = args.next()
        .ok_or_else(|| eyre!("{}", usage))?;
    let path = Path::new(&path);
    let mut shows = BTreeMap::new();
    for child in directories_in(path)? {
        let name = match child.file_name().to_str() {
            Some(x) => x.to_owned(),
            None => {
                eprintln!("skipping non-Unicode path {:?}", child.path());
                continue;
            }
        };
        shows.insert(name.clone(), Show { name: name.clone(), episodes: Vec::default() });
    }

    let mut wrapper = MediaInfo::new();
    for (name, show) in shows.iter_mut() {
        eprintln!("show {}", name);
        show.episodes = find_episodes(&mut wrapper, show, path)?;
        debug_dbg!(show);
    }

    let path = args.next()
        .ok_or_else(|| eyre!("{}", usage))?;
    let path = Path::new(&path);
    let ads = find_ads(&mut wrapper, path)?;

    let config = Config { version: 1, shows, ads };
    let mut json = BufWriter::new(File::create("cursedtv.json")?);
    json.write_all(serde_json::to_string(&config)?.as_bytes())?;

    Ok(config)
}

fn wrap_mediainfo_error(inner: MediaInfoError) -> eyre::Report {
    eyre!("mediainfo error: {:?}", inner)
}

fn check_child(wrapper: &mut MediaInfo, child: DirEntry) -> eyre::Result<Option<(PathBuf, Duration)>> {
    if !child.file_type()?.is_file() {
        return Ok(None);
    }

    debug_dbg!(child.path());
    debug_dbg!(wrapper.open(&child.path()).map_err(wrap_mediainfo_error)?);
    // let streams: Option<_> = wrapper.video_streams();
    // let stream = streams.and_then(|x| x.first());
    // debug_dbg!(stream.map(|x| x.duration()));

    let output = match wrapper.inform() {
        Err(MediaInfoError::ZeroLengthResultError) => return Ok(None),
        x => x,
    };
    let output = output.map_err(wrap_mediainfo_error)?;
    let line = match output.lines().find(|x| x.starts_with("Duration  ")) {
        Some(x) => x,
        None => return Ok(None), // mediainfo output has no Duration line
    };
    let (_, value) = line.split_once(": ")
        .ok_or_else(|| eyre!("mediainfo Duration line has no colon"))?;
    eprintln!("duration raw: {:?}", value);
    let parts = value.split(" ").chunks(2);
    let mut duration = Duration::ZERO;
    for mut part in &parts {
        duration += match (part.next(), part.next()) {
            (Some(x), Some("h")) => Duration::from_secs(u64::from_str(x)? * 3600),
            (Some(x), Some("min")) => Duration::from_secs(u64::from_str(x)? * 60),
            (Some(x), Some("s")) => Duration::from_secs(u64::from_str(x)?),
            (Some(x), Some("ms")) => Duration::from_millis(u64::from_str(x)?),
            _ => bail!("mediainfo Duration line has bad format: {}", value),
        };
    }

    Ok(Some((child.path(), duration)))
}

fn find_episodes(wrapper: &mut MediaInfo, show: &mut Show, root: &Path) -> eyre::Result<Vec<Video>> {
    let mut result = vec![];
    let directories = show.directories(root)?;
    debug_dbg!(&directories);

    for directory in directories {
        for child in read_dir(directory)? {
            if let Some((path, duration)) = check_child(wrapper, child?)? {
                let path = match path.strip_prefix(&root)?.strip_prefix(&show.name)?.to_str() {
                    Some(x) => x,
                    None => {
                        eprintln!("skipping non-Unicode path {:?}", path);
                        continue;
                    }
                };
                eprintln!("episode {}", path);
                let ms = duration.as_millis();
                eprintln!("    duration: {}.{:03} s", ms / 1000, ms % 1000);

                result.push(Video { path: path.to_owned(), duration });
            }
        }
    }

    Ok(result)
}

fn find_ads(wrapper: &mut MediaInfo, root: &Path) -> eyre::Result<Vec<Video>> {
    let mut result = vec![];

    for child in read_dir(root)? {
        if let Some((path, duration)) = check_child(wrapper, child?)? {
            let path = match path.strip_prefix(&root)?.to_str() {
                Some(x) => x,
                None => {
                    eprintln!("skipping non-Unicode path {:?}", path);
                    continue;
                }
            };
            eprintln!("ad {}", path);
            let ms = duration.as_millis();
            eprintln!("    duration: {}.{:03} s", ms / 1000, ms % 1000);

            result.push(Video { path: path.to_owned(), duration });
        }
    }

    Ok(result)
}

impl Show {
    fn directories(&self, path: &Path) -> eyre::Result<Vec<PathBuf>> {
        let mut result = vec![path.join(&self.name)];
        for dir in directories_in_recursive(&path.join(&self.name))? {
            result.push(dir.path());
        }

        Ok(result)
    }
}
